import pytest


@pytest.fixture
def mk_income_measure_type(
    fixture_factory,
    income_statement_measure_type_categories,
):
    from endi.models.accounting.income_statement_measures import (
        IncomeStatementMeasureType,
    )

    return fixture_factory(
        IncomeStatementMeasureType,
        category=income_statement_measure_type_categories[0],
        is_total=False,
        total_type="",
    )
