import os


API_COMPANY_COLLECTION_ROUTE = "/api/v1/companies/{id}/estimations"
API_ADD_ROUTE = os.path.join(API_COMPANY_COLLECTION_ROUTE, "add")
API_COLLECTION_ROUTE = "/api/v1/estimations"
API_ITEM_ROUTE = os.path.join(API_COLLECTION_ROUTE, "{id}")

ESTIMATION_COLLECTION_ROUTE = "/estimations"
ESTIMATION_ITEM_ROUTE = "/estimations/{id}"


def includeme(config):
    config.add_route(API_COLLECTION_ROUTE, API_COLLECTION_ROUTE)
    config.add_route(ESTIMATION_COLLECTION_ROUTE, ESTIMATION_COLLECTION_ROUTE)

    for route in API_COMPANY_COLLECTION_ROUTE, API_ADD_ROUTE:
        config.add_route(route, route, traverse="/companies/{id}")

    for route in (API_ITEM_ROUTE, ESTIMATION_ITEM_ROUTE):
        # On assure qu'on matche la route qui finit par un id et pas id.html par exemple
        pattern = r"{}".format(route.replace("id", r"id:\d+"))
        config.add_route(route, pattern, traverse="/tasks/{id}")
