from endi.views import (
    BaseCsvView,
)

from endi.views.accounting.company_general_ledger import (
    CompanyGeneralLedgerOperationsListTools,
)

from endi.views.accounting.operations import (
    OperationListTools,
)

from sqla_inspect.csv import CsvExporter
from sqla_inspect.excel import XlsExporter
from sqla_inspect.ods import OdsExporter


def create_company_general_ledger_operations_view(exporter_class, extension):
    class Writer(exporter_class):
        headers = (
            {"name": "general_account_number", "label": "Compte comptable"},
            {"name": "general_account_name", "label": "Nom du compte"},
            {"name": "date", "label": "Date"},
            {"name": "label", "label": "Libellé"},
            {"name": "debit", "label": "Debit"},
            {"name": "credit", "label": "Crédit"},
            {"name": "balance", "label": "Solde"},
        )

    class ExportView(CompanyGeneralLedgerOperationsListTools, BaseCsvView):
        writer = Writer
        filename = "grand-livre.{extension}".format(extension=extension)

        def _init_writer(self):
            return self.writer()

        def _stream_rows(self, query):
            wording = self.get_wording_dict()

            for operation in query.all():
                yield {
                    "general_account_number": operation.general_account,
                    "general_account_name": wording.get(operation.general_account, ""),
                    "label": operation.label,
                    "date": operation.date,
                    "debit": operation.debit,
                    "credit": operation.credit,
                    "balance": operation.balance,
                }

    return ExportView


def create_admin_operations_view(exporter_class, extension):
    class Writer(exporter_class):
        headers = (
            {"name": "general_account_number", "label": "Compte comptable"},
            {"name": "general_account_name", "label": "Nom du compte"},
            {"name": "date", "label": "Date"},
            {"name": "label", "label": "Libellé"},
            {"name": "debit", "label": "Debit"},
            {"name": "credit", "label": "Crédit"},
            {"name": "balance", "label": "Solde"},
        )

    class ExportView(OperationListTools, BaseCsvView):
        writer = Writer
        filename = "operations.{extension}".format(extension=extension)

        def _init_writer(self):
            return self.writer()

        def _stream_rows(self, query):
            for operation in query.all():
                yield {
                    "analytical_account": operation.analytical_account,
                    "general_account_number": operation.general_account,
                    "label": operation.label,
                    "date": operation.date,
                    "debit": operation.debit,
                    "credit": operation.credit,
                    "balance": operation.balance,
                }

    return ExportView


def includeme(config):
    config.add_view(
        create_company_general_ledger_operations_view(CsvExporter, "csv"),
        route_name="grand_livre.{extension}",
        match_param="extension=csv",
        permission="view.accounting",
    )
    config.add_view(
        create_company_general_ledger_operations_view(XlsExporter, "xls"),
        route_name="grand_livre.{extension}",
        match_param="extension=xls",
        permission="view.accounting",
    )
    config.add_view(
        create_company_general_ledger_operations_view(OdsExporter, "ods"),
        route_name="grand_livre.{extension}",
        match_param="extension=ods",
        permission="view.accounting",
    )
    config.add_view(
        create_admin_operations_view(CsvExporter, "csv"),
        route_name="operations.{extension}",
        match_param="extension=csv",
        permission="admin_accounting",
    )
    config.add_view(
        create_admin_operations_view(XlsExporter, "xls"),
        route_name="operations.{extension}",
        match_param="extension=xls",
        permission="admin_accounting",
    )
    config.add_view(
        create_admin_operations_view(OdsExporter, "ods"),
        route_name="operations.{extension}",
        match_param="extension=ods",
        permission="admin_accounting",
    )
