import logging
import datetime
import colander
from collections import OrderedDict
from sqlalchemy.orm import (
    joinedload,
)

from endi.export.accounting_spreadsheet import (
    CellsIndex,
    SpreadSheetCompiler,
    SpreadSheetSyntax,
    XLSXSyntax,
    ODSSyntax,
)
from endi.export.utils import write_file_to_request
from endi.export.excel import XlsExporter
from endi.export.ods import OdsExporter
from endi.compute import math_utils
from endi.utils.widgets import Link
from endi.utils.strings import (
    short_month_name,
    format_float,
)
from endi.models.company import Company
from endi.models.accounting.income_statement_measures import (
    IncomeStatementMeasureGrid,
    IncomeStatementMeasure,
    IncomeStatementMeasureTypeCategory,
)
from endi.forms.accounting import (
    get_income_statement_measures_list_schema,
    get_upload_treasury_list_schema,
)
from endi.views import BaseListView
from endi.views.accounting.routes import (
    UPLOAD_ITEM_ROUTE,
    UPLOAD_ITEM_INCOME_STATEMENT_ROUTE,
    INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
    INCOME_STATEMENT_GRIDS_ROUTE,
)

logger = logging.getLogger(__name__)


class IncomeStatementMeasureGridListView(BaseListView):
    """
    List companies having IncomeStatementMeasureGrid generated with the current
    context (AccountingOperationUpload)
    """

    sort_columns = {
        "company": "name",
    }
    add_template_vars = ("stream_actions",)
    default_sort = "company"
    default_direction = "asc"
    schema = get_upload_treasury_list_schema()
    title = "Liste des comptes de résultat"

    def populate_actionmenu(self, appstruct):
        self.request.navigation.breadcrumb.append(
            Link(
                self.request.route_path(UPLOAD_ITEM_ROUTE, id=self.context.id),
                "Revenir à la liste des écritures",
            )
        )
        self.request.navigation.breadcrumb.append(Link("", self.title))

    def query(self):
        query = Company.query().filter(
            Company.id.in_(
                self.dbsession.query(IncomeStatementMeasureGrid.company_id).filter_by(
                    upload_id=self.context.id
                )
            )
        )
        return query

    def filter_company_id(self, query, appstruct):
        company_id = appstruct.get("company_id")
        if company_id not in (colander.null, None):
            query = query.filter_by(id=company_id)
        return query

    def stream_actions(self, company):
        url = self.request.route_path(INCOME_STATEMENT_GRIDS_ROUTE, id=company.id)
        return (
            Link(
                url,
                "Voir ce compte de résultat",
                title="Voir le détail de ce compte de résultats",
                icon="euro-circle",
                css="icon",
            ),
        )


class YearGlobalGrid:
    """
    Abstract class used to modelize the income statement and group all stuff
    """

    def __init__(self, grids, turnover):
        if not grids:
            self.is_void = True
        else:
            self.is_void = False

        if not self.is_void:
            self.categories = IncomeStatementMeasureTypeCategory.get_categories()

            self.turnover = turnover
            self.build_indexes(grids)
            self.rows = list(self.compile_rows())

    def build_indexes(self, grids):
        # Month grids stored by month number
        self.grids = self._grid_by_month(grids)

        # Types by category id
        self.types = self._type_by_category()

    @staticmethod
    def _grid_by_month(month_grids):
        """
        Store month grids by month
        """
        result = OrderedDict((month, None) for month in range(1, 13))
        for grid in month_grids:
            result[grid.month] = grid
        return result

    def _get_last_filled_grid(self):
        """
        Return the last grid (month) where datas were filled
        In fact if we pass here, there should be almost one

        :returns: An IncomeStatementMeasureGrid
        """
        result = None
        grids = list(self.grids.values())
        grids.reverse()
        for grid in grids:
            if grid is not None:
                result = grid
                break
        return result

    def _type_by_category(self):
        """
        Stores IncomeStatementMeasureType by category (to keep the display
        order)

        :returns: A dict {'category.id': [IncomeStatementMeasureType]}
        :rtype: dict
        """
        result = dict((category.id, []) for category in self.categories)
        # On  est sûr d'avoir au moins une grille
        last_grid = self._get_last_filled_grid()
        if not last_grid:
            logger.error("All grids are void")
            return result
        types = IncomeStatementMeasure.get_measure_types(last_grid.id)

        for type_ in types:
            # Les types donc la catégorie a été désactivé entre temps doit
            # toujours apparaitre
            if type_.category not in self.categories:
                self.categories.append(type_.category)
                result[type_.category.id] = []
            result[type_.category.id].append(type_)
        return result

    def _get_month_cell(self, grid, type_id):
        """
        Return the value to display in month related cells
        """
        result = 0

        if grid is not None:
            measure = grid.get_measure_by_type(type_id)
            if measure is not None:
                result = measure.get_value()

        return result

    def compile_rows(self):
        """
        Collect the output grid corresponding to the current grid list

        Stores all datas in rows (each row matches a measure_type)
        Compute totals and ratio per line, and an indicator to tell if a line is full of zeroes.

        :returns: generator yielding 3-uples (type, row, contains_only_zeroes) where type is a
        IncomeStatementMeasureType instance and row contains the datas of the
        grid row for the given type (15 columns).
        :rtype: tuple
        """
        for category in self.categories:
            for type_ in self.types[category.id]:
                row = []
                sum = 0
                contains_only_zeroes = True
                for month, grid in list(self.grids.items()):
                    value = self._get_month_cell(grid, type_.id)
                    if value != 0:
                        contains_only_zeroes = False
                    sum += value
                    row.append(value)

                row.append(sum)
                percent = math_utils.percent(sum, self.turnover, 0)
                row.append(percent)

                yield type_, contains_only_zeroes, row

    def format_datas(self):
        """
        Format all numeric datas to strings in localized formats
        """
        for row in self.rows:
            for index, data in enumerate(row[2]):
                row[2][index] = format_float(data, precision=2, wrap_decimals=True)

    def get_updated_at(self):
        """
        Return the last date of the data after a celery update
        """
        dates_updated_at = []
        for month, grid in self.grids.items():
            if grid is not None:
                dates_updated_at.append(grid.updated_at)

        logger.debug(dates_updated_at)

        # Get rid of None values
        dates_updated_at = list(filter(None, dates_updated_at))

        if not dates_updated_at:
            return None
        else:
            return max(dates_updated_at)


class YearGlobalGridWithFormulas(YearGlobalGrid):
    """
    Like YearGlobalGrid, but will generate totals as spreadsheet formulas instead of numbers.

    Internaly, passes `SpreadsheetFormula` instances instead of `str` to allow exporter to distinguish a formula from an str.
    """

    def __init__(self, syntax: SpreadSheetSyntax, *args, **kwargs):
        self.syntax = syntax
        super().__init__(*args, **kwargs)

    def format_datas(self):
        pass

    def build_indexes(self, grids):
        super().build_indexes(grids)

        # Index of indicator and category members rows.
        self.cells_index = CellsIndex()

        # Use this same iteration despite the 1st level of iteration being useless to
        # keep the same order
        for category, types in self.types.items():
            for type_ in types:
                self.cells_index.register(type_)

    def compile_rows(self):
        """
        Collect the output grid corresponding to the current grid list

        Stores all datas in rows (each row matches a measure_type)
        Compute totals and ratio per line

        :returns: generator yielding 2-uple (type, row) where type is a
        IncomeStatementMeasureType instance and row contains the datas of the
        grid row for the given type (15 columns).
        :rtype: tuple
        """

        compiler = SpreadSheetCompiler(
            self.syntax,
            self.cells_index,
            x_offset=1,  # skip: label col
            y_offset=3,  # skip: Title col, blank col, month name col
        )

        # Flattens the list of types that are stored by category
        types = (
            type_ for category in self.categories for type_ in self.types[category.id]
        )

        for type_index, type_ in enumerate(types):
            row = []
            contains_only_zeroes = True
            for month, grid in list(self.grids.items()):
                value = self._get_month_cell(grid, type_.id, compiler)
                row.append(value)
                if value != 0:
                    contains_only_zeroes = False

            sum_formula = compiler.get_row_sum_formula(type_index, len(row))
            sum_xy_coordinates = len(row), type_index

            percentage_formula = compiler.get_row_percentage_formula(
                sum_xy_coordinates, self.turnover
            )

            row.append(sum_formula)
            row.append(percentage_formula)

            yield type_, contains_only_zeroes, row

    def _get_month_cell(
        self, grid: IncomeStatementMeasureGrid, type_id, compiler: SpreadSheetCompiler
    ):
        """
        Return the value to display in month related cells
        """
        result = 0

        if grid is not None:
            measure = grid.get_measure_by_type(type_id)
            if measure is not None:
                if measure.measure_type.is_computed_total:
                    # index from 0 (January is stored as 1, and will have col index 0)
                    x_coordinate = grid.month - 1
                    result = compiler.get_column_formula(
                        measure.measure_type, x_coordinate
                    )
                else:
                    result = measure.get_value()

        return result


class CompanyIncomeStatementMeasuresListView(BaseListView):
    schema = get_income_statement_measures_list_schema()
    use_paginate = False
    default_sort = "month"
    sort_columns = {"month": "month"}
    filter_button_label = "Changer"
    filter_button_icon = False
    filter_button_css = "btn btn-primary"

    title = "Compte de résultat"

    def query(self):
        """
        Collect the grids we present in the output
        """
        query = self.request.dbsession.query(IncomeStatementMeasureGrid)
        query = query.options(
            joinedload(IncomeStatementMeasureGrid.measures, innerjoin=True)
        )
        query = query.filter(IncomeStatementMeasureGrid.company_id == self.context.id)
        return query

    def filter_year(self, query, appstruct):
        """
        Filter the current query by a given year
        """
        year = appstruct.get("year")
        logger.debug("Filtering by year : %s" % year)

        if year not in (None, colander.null):
            query = query.filter(IncomeStatementMeasureGrid.year == year)
            self.year = year
        else:
            self.year = datetime.date.today().year
        return query

    def more_template_vars(self, response_dict):
        """
        Add template datas in the response dictionnary
        """
        month_grids = response_dict["records"]
        logger.debug("MONTH : {}".format(month_grids.count()))
        year_turnover = self.context.get_turnover(
            "{}0101".format(self.year),
            "{}1231".format(self.year),
        )

        grid = YearGlobalGrid(month_grids, year_turnover)
        grid.format_datas()
        response_dict["grid"] = grid
        response_dict["current_year"] = datetime.date.today().year
        response_dict["selected_year"] = int(self.year)
        response_dict["show_zero_rows"] = self.appstruct.get("show_zero_rows")
        response_dict["show_decimals"] = self.appstruct.get("show_decimals")
        response_dict["export_xls_url"] = self.request.route_path(
            INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
            id=self.context.id,
            extension="xls",
            _query=self.request.GET,
        )
        response_dict["export_ods_url"] = self.request.route_path(
            INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
            id=self.context.id,
            extension="ods",
            _query=self.request.GET,
        )
        return response_dict


class IncomeStatementMeasureGridXlsView(CompanyIncomeStatementMeasuresListView):
    """
    Xls output
    """

    _factory = XlsExporter
    syntax = XLSXSyntax()

    @property
    def filename(self):
        return "compte_de_resultat_{}.{}".format(
            self.year,
            self.request.matchdict["extension"],
        )

    def _stream_rows(self, query):
        year_turnover = self.context.get_turnover(
            "{}0101".format(self.year),
            "{}1231".format(self.year),
        )
        grid = YearGlobalGridWithFormulas(self.syntax, query, year_turnover)
        yield from grid.rows

    def _init_writer(self, writer_options=None):
        writer = self._factory(options=writer_options)
        writer.add_title(
            "Compte de résultat de {} pour l’année {}".format(
                self.context.name,
                self.year,
            ),
            width=15,
        )
        writer.add_breakline()
        headers = [short_month_name(i).capitalize() for i in range(1, 13)]
        headers.insert(0, "")
        headers.append("TOTAL")
        headers.append("% CA")
        writer.add_headers(headers)
        return writer

    def _mk_writer_options(self, appstruct):
        if appstruct.get("show_decimals", False):
            decimal_places = "2"
        else:
            decimal_places = "0"
        return {"decimal_places": decimal_places}

    def _build_return_value(self, schema, appstruct, query):
        writer = self._init_writer(self._mk_writer_options(appstruct))
        writer._datas = []
        for type_, contains_only_zeroes, row in self._stream_rows(query):

            row_options = {
                "hidden": not appstruct.get("show_zero_rows") and contains_only_zeroes,
                "highlight": type_.is_total,
            }
            row_datas = [type_.label]
            row_datas.extend(row)

            writer.add_row(row_datas, options=row_options)
        writer.set_column_options(column_index=0, column_style_name="wide_column")
        write_file_to_request(self.request, self.filename, writer.render())
        return self.request.response


class IncomeStatementMeasureGridOdsView(IncomeStatementMeasureGridXlsView):
    _factory = OdsExporter
    syntax = ODSSyntax()


def includeme(config):
    config.add_view(
        IncomeStatementMeasureGridListView,
        route_name=UPLOAD_ITEM_INCOME_STATEMENT_ROUTE,
        permission="admin_accounting",
        renderer="/accounting/income_statement_grids.mako",
    )
    config.add_view(
        CompanyIncomeStatementMeasuresListView,
        route_name=INCOME_STATEMENT_GRIDS_ROUTE,
        permission="view.accounting",
        renderer="/accounting/income_statement_measures.mako",
    )
    config.add_view(
        IncomeStatementMeasureGridXlsView,
        route_name=INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
        permission="view.accounting",
        match_param="extension=xls",
    )
    config.add_view(
        IncomeStatementMeasureGridOdsView,
        route_name=INCOME_STATEMENT_GRIDS_ROUTE_EXPORT,
        permission="view.accounting",
        match_param="extension=ods",
    )

    config.add_company_menu(
        parent="accounting",
        order=1,
        label="Comptes de résultat",
        route_name=INCOME_STATEMENT_GRIDS_ROUTE,
        route_id_key="company_id",
    )
