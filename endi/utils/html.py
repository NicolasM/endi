import bleach
import lxml.html
import lxml.etree
from copy import deepcopy


ALLOWED_HTML_TAGS = bleach.ALLOWED_TAGS + [
    "font",
    "br",
    "p",
    "span",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "hr",
    "img",
    "div",
    "pre",
    "sup",
    "u",
    "strike",
    "sub",
    "code",
    "sup",
]
ALLOWED_HTML_ATTRS = deepcopy(bleach.ALLOWED_ATTRIBUTES)
ALLOWED_HTML_ATTRS["font"] = ["color"]
ALLOWED_HTML_ATTRS["*"] = ["class", "style"]
ALLOWED_HTML_ATTRS["img"] = ["src", "width", "height", "alt"]
ALLOWED_HTML_ATTRS["a"] = ["href", "target", "download"]
ALLOWED_CSS_STYLES = [
    "text-align",
    "font-weight",
    "font-family",
    "text-decoration",
    "padding",
    "padding-left",
    "color",
    "background-color",
    "line-height",
]


VOID_TAGS = (
    "<br />",
    "<br/>",
)
TAGS_TO_CHECK = (
    ("<p>", "</p>"),
    ("<div>", "</div>"),
)


def remove_tag(text, tag):
    """
    Remove the tag from the beginning of the given text

    :param str text: The text with the tag
    :param str tag: The tag to remove
    :rtype: str
    """
    return text[0 : -1 * len(tag)].strip()


def strip_whitespace(value):
    """
    Strip whitespace and tabs at the beginning/end of a string

    :param str value: The value to clean
    :rtype: str
    """
    if hasattr(value, "strip"):
        value = value.strip(" \t")
    return value


def strip_linebreaks(value):
    """
    Strip linebreaks

    :param str value: The value to clean
    :rtype: str
    """
    # we don't use rstrip since it's used for character stripping
    # (not chain)
    if hasattr(value, "strip"):
        value = value.strip("\n\r")
        for tag in "<br />", "<br>", "<br/>":
            if value.endswith(tag):
                value = remove_tag(value, tag)
                return strip_linebreaks(value)

    return value


def strip_void_lines(value):
    """
    RStrip value ending with void html tags

    :param str value: The value to clean
    :rtype: str
    """
    if hasattr(value, "strip"):
        for tag, close_tag in TAGS_TO_CHECK:
            if value.endswith(close_tag):
                prec_value = remove_tag(value, close_tag)
                prec_value = strip_whitespace(prec_value)
                prec_value = strip_linebreaks(prec_value)
                if prec_value.endswith(tag):
                    value = remove_tag(prec_value, tag)
                    value = strip_whitespace(value)
                    value = strip_linebreaks(value)
                    return strip_void_lines(value)

    return value


def strip_html(value):
    """
    Strip html void lines
    """
    value = strip_whitespace(value)
    value = strip_linebreaks(value)
    return strip_void_lines(value)


def strip_html_tags(text):
    return bleach.clean(text, tags=[], strip=True)


def clean_html(text):
    """
    Return a sanitized version of an html code keeping essential html tags
    and allowing only a few attributes
    """
    if text:
        text = strip_html(text)
        return bleach.clean(
            text,
            tags=ALLOWED_HTML_TAGS,
            attributes=ALLOWED_HTML_ATTRS,
            styles=ALLOWED_CSS_STYLES,
            strip=True,
        )
    else:
        return text


def split_rich_text_in_blocks(html_text: str):
    """
    Split the html text around the first level occurences of tagname

    >>> split_by_first_level_tag("<p>text <p>Texte</p></p><p>Texte2</p>texte en dehors"
    ['<p>text <p>Texte</p></p>', '<p>Texte2</p>', 'text en dehors]
    """
    if html_text is None:
        return [""]

    html_text = clean_html(html_text)

    try:
        # on a une balise parente
        lxml.html.fragment_fromstring(html_text)
        single = True
    except lxml.etree.ParserError:
        # On a pas de balise parent, on subdivise
        # PLusieurs résultats
        surrounding = lxml.html.fragment_fromstring(html_text, create_parent="div")
        single = False

    # On a une balise html au niveau du dessus, on ne peut pas splitter
    if single:
        return [html_text]
    elif len(surrounding.getchildren()) <= 1:
        return [html_text]
    else:
        # le résultat c'est les balises de premier niveau et d'éventuel textes
        # placés au premier niveau également
        result = []
        children = surrounding.getchildren()
        for child in children:
            result.append(lxml.etree.tostring(child).decode("utf-8"))
        return result
