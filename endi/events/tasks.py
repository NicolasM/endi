"""
Handle task (invoice/estimation) related events
"""
import logging
import typing

from pyramid_mailer.message import Attachment

from endi_base.mail import (
    format_link,
)

from endi.views.task.utils import get_task_url
from endi.utils.notification import AbstractNotification, notify
from endi.utils.strings import (
    format_status,
    format_account,
)
from endi.models.files import File


logger = logging.getLogger(__name__)

# Events for which a mail will be sended
EVENTS = {
    "valid": "validé",
    "invalid": "invalidé",
    "paid": "partiellement payé",
    "resulted": "payé",
}

SUBJECT_TMPL = "{docname} ({customer}) : {statusstr}"

MAIL_TMPL = """\
Bonjour {username},

{docname} {docnumber} du dossier {project} avec le client {customer} \
a été {status_verb}{gender}.

Vous pouvez {determinant} consulter ici :
{addr}

Commentaires associés au document :
    {comment}"""


def get_title(event) -> str:
    """
    return the subject of the email
    """
    return SUBJECT_TMPL.format(
        docname=event.node.name,
        customer=event.node.customer.label,
        statusstr=format_status(event.node),
    )


def get_status_verb(status) -> str:
    """
    Return the verb associated to the current status
    """
    return EVENTS.get(status, "")


def get_body(event) -> str:
    """
    return the body of the email
    """
    status_verb = get_status_verb(event.status)

    # If the document is validated, we directly send the link to the pdf
    # file
    if event.status == "valid":
        suffix = ".pdf"
    else:
        suffix = ""

    addr = get_task_url(
        event.request,
        event.node,
        suffix=suffix,
        absolute=True,
    )
    addr = format_link(event.get_settings(), addr)

    docnumber = event.node.internal_number.lower()
    customer = event.node.customer.label
    project = event.node.project.name.capitalize()
    if event.node.type_ == "invoice":
        docname = "La facture"
        gender = "e"
        determinant = "la"
    elif event.node.type_ == "internalinvoice":
        docname = "La facture interne"
        gender = "e"
        determinant = "la"
    elif event.node.type_ == "cancelinvoice":
        docname = "L'avoir"
        gender = ""
        determinant = "le"
    elif event.node.type_ == "estimation":
        docname = "Le devis"
        gender = ""
        determinant = "le"
    elif event.node.type_ == "internalestimation":
        docname = "Le devis interne"
        gender = ""
        determinant = "le"
    else:
        determinant = ""
        docname = "Inconnu"
        gender = ""
    if event.node.status_comment:
        comment = event.node.status_comment
    else:
        comment = "Aucun"

    username = format_account(event.node.owner, reverse=False)
    return MAIL_TMPL.format(
        determinant=determinant,
        username=username,
        docname=docname,
        docnumber=docnumber,
        customer=customer,
        project=project,
        status_verb=status_verb,
        gender=gender,
        addr=addr,
        comment=comment,
    )


def get_notification(event) -> AbstractNotification:
    return AbstractNotification(
        key=f"task:status:{event.status}",
        title=get_title(event),
        body=get_body(event),
    )


def get_attachment(document) -> typing.Optional[Attachment]:
    """Return the file to be attached to the email"""
    if document.pdf_file:
        file: File = document.pdf_file
        value = file.getvalue()
        if value is not None:
            return Attachment(
                file.name,
                "application/pdf",
                file.getvalue(),
            )


def notify_task_status_changed(event):
    """Notify end users when task status changed"""
    # Silly hack :
    # When a payment is registered, the new status is "paid",
    # if the resulted box has been checked, it's set to resulted later on.
    # So here, we got the paid status, but in reality, the status has
    # already been set to resulted. This hack avoid to send emails with the
    # wrong message
    if event.status == "paid" and event.node.paid_status == "resulted":
        event.status = "resulted"

    if event.status not in list(EVENTS.keys()):
        return

    attachment = None
    if event.status == "valid":
        attachment = get_attachment(event.node)
    notify(
        event.request,
        get_notification(event),
        company_id=event.node.company_id,
        attachment=attachment,
    )


def on_status_changed_alert_related_business(event):
    """
    Alert the related business on Invoice status change

    :param event: A StatusChanged instance with an Invoice attached
    """
    business = event.node.business
    if business is not None:
        logger.info(
            "+ Status Changed : updating business {} invoicing status".format(
                business.id
            )
        )
        business.status_service.on_task_status_change(
            business, event.node, event.status
        )
