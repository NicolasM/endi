<%doc>
Toggle link template
</%doc>
% if elem.permitted(request.context, request):
   <a title='${elem.title}' aria-label='${elem.title}' href="#" data-toggle='collapse' data-target='#${elem.target}'
   % if elem.css:
    class="${elem.css}"
   % endif
   >
   %if elem.icon:
       <svg><use href="${request.static_url('endi:static/icons/endi.svg')}#${elem.icon}"></use></svg>
   % endif
   ${elem.label}
  </a>
% endif
