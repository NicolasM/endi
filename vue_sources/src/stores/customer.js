import getFormConfigStore from './formConfig'
import getModelStore from './modelStore.ts'

export const useCustomerStore = getModelStore('customer')
export const useCustomerConfigStore = getFormConfigStore('customer')
