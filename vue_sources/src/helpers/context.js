import http from "./http";

export const collectOptions = () => AppOption;

export const getAssetPath = (asset) => AppOption['static_path'] + asset;
