/**
 *
 * Http Request tools
 *
 * Should be used to process GET/POST/PUT/DELETE requests from within endi vue js code
 *
 * Here we can manage custom headers or login errors ...
 *
 * import {http} from '@/helpers/http.js';
 *
 * http.get(url, [data]);
 * http.post(url, data);
 * http.put(url, data);
 * http.delete(url, [data]);
 */

import { collectOptions } from './context'

function addCsrfToken(url, headers, token) {
  if (!(/^http:.*/.test(url) || /^https:.*/.test(url))) {
    headers['X-CSRFToken'] = token
  }
}

function request(method) {
  return (url, body) => {
    const requestOptions = {
      method,
      // Ici on pourrait avoir des headers
      headers: {},
    }

    if (body) {
      requestOptions.headers['Content-Type'] = 'application/json'

      const options = collectOptions()
      if (options['csrf_token']) {
        console.log('Adding csrf token to headers')
        addCsrfToken(url, requestOptions.headers, options['csrf_token'])
        console.log('Adding csrf token to data')
        body['csrf_token'] = options['csrf_token']
      }
      requestOptions.body = JSON.stringify(body)
    }
    return fetch(url, requestOptions).then(handleResponse, handleResponse)
  }
}

async function handleResponse(response) {
  const isJson = response.headers
    ?.get('content-type')
    ?.includes('application/json')
  const data = isJson ? await response.json() : null

  // check for error response
  if (!response.ok) {
    // get error message from body or default to response status
    let error = (data && data.errors) || response.status
    if (response.status == 401) {
      error = "Cette page n'existe pas"
    } else if (response.status == 403) {
      error = "Vous n'êtes plus authentifié"
    }
    return Promise.reject(error)
  }
  return data
}

const http = {
  get: request('GET'),
  post: request('POST'),
  put: request('PUT'),
  delete: request('DELETE'),
}
export default http
