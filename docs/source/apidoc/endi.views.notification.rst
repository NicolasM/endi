endi.views.notification package
===============================

Submodules
----------

endi.views.notification.rest\_api module
----------------------------------------

.. automodule:: endi.views.notification.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.notification.routes module
-------------------------------------

.. automodule:: endi.views.notification.routes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.notification
   :members:
   :undoc-members:
   :show-inheritance:
