endi.plugins.sap\_urssaf3p.views.invoices package
=================================================

Submodules
----------

endi.plugins.sap\_urssaf3p.views.invoices.invoice module
--------------------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.views.invoices.invoice
   :members:
   :undoc-members:
   :show-inheritance:

endi.plugins.sap\_urssaf3p.views.invoices.lists module
------------------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.views.invoices.lists
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap_urssaf3p.views.invoices
   :members:
   :undoc-members:
   :show-inheritance:
