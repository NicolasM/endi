endi.tests.views.admin.sale package
===================================

Submodules
----------

endi.tests.views.admin.sale.test\_accounting module
---------------------------------------------------

.. automodule:: endi.tests.views.admin.sale.test_accounting
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.admin.sale.test\_forms module
----------------------------------------------

.. automodule:: endi.tests.views.admin.sale.test_forms
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.admin.sale.test\_tva module
--------------------------------------------

.. automodule:: endi.tests.views.admin.sale.test_tva
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.admin.sale
   :members:
   :undoc-members:
   :show-inheritance:
