endi.plugins.sap\_urssaf3p.views.admin package
==============================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.plugins.sap_urssaf3p.views.admin.sale
   endi.plugins.sap_urssaf3p.views.admin.sap

Module contents
---------------

.. automodule:: endi.plugins.sap_urssaf3p.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
