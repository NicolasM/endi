endi.tests package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.tests.compute
   endi.tests.endi_celery
   endi.tests.endi_payment
   endi.tests.forms
   endi.tests.models
   endi.tests.panels
   endi.tests.plugins
   endi.tests.utils
   endi.tests.views

Submodules
----------

endi.tests.base module
----------------------

.. automodule:: endi.tests.base
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.conftest module
--------------------------

.. automodule:: endi.tests.conftest
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.test\_session module
-------------------------------

.. automodule:: endi.tests.test_session
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.tools module
-----------------------

.. automodule:: endi.tests.tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests
   :members:
   :undoc-members:
   :show-inheritance:
