endi.views.statistics package
=============================

Submodules
----------

endi.views.statistics.rest\_api module
--------------------------------------

.. automodule:: endi.views.statistics.rest_api
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.statistics.routes module
-----------------------------------

.. automodule:: endi.views.statistics.routes
   :members:
   :undoc-members:
   :show-inheritance:

endi.views.statistics.statistics module
---------------------------------------

.. automodule:: endi.views.statistics.statistics
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.views.statistics
   :members:
   :undoc-members:
   :show-inheritance:
