endi.tests.views.userdatas package
==================================

Submodules
----------

endi.tests.views.userdatas.test\_py3o module
--------------------------------------------

.. automodule:: endi.tests.views.userdatas.test_py3o
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.userdatas.test\_userdatas module
-------------------------------------------------

.. automodule:: endi.tests.views.userdatas.test_userdatas
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.userdatas
   :members:
   :undoc-members:
   :show-inheritance:
