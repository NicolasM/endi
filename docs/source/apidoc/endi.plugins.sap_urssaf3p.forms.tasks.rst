endi.plugins.sap\_urssaf3p.forms.tasks package
==============================================

Submodules
----------

endi.plugins.sap\_urssaf3p.forms.tasks.invoice module
-----------------------------------------------------

.. automodule:: endi.plugins.sap_urssaf3p.forms.tasks.invoice
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap_urssaf3p.forms.tasks
   :members:
   :undoc-members:
   :show-inheritance:
