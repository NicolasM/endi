endi.tests.views.notification package
=====================================

Submodules
----------

endi.tests.views.notification.test\_rest\_api module
----------------------------------------------------

.. automodule:: endi.tests.views.notification.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.notification
   :members:
   :undoc-members:
   :show-inheritance:
