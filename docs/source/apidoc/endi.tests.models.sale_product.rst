endi.tests.models.sale\_product package
=======================================

Submodules
----------

endi.tests.models.sale\_product.test\_services module
-----------------------------------------------------

.. automodule:: endi.tests.models.sale_product.test_services
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.sale\_product.test\_work\_item module
-------------------------------------------------------

.. automodule:: endi.tests.models.sale_product.test_work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.models.sale_product
   :members:
   :undoc-members:
   :show-inheritance:
