endi.panels.project package
===========================

Submodules
----------

endi.panels.project.business\_metrics\_mixins module
----------------------------------------------------

.. automodule:: endi.panels.project.business_metrics_mixins
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.project.phase module
--------------------------------

.. automodule:: endi.panels.project.phase
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.project.type module
-------------------------------

.. automodule:: endi.panels.project.type
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.panels.project
   :members:
   :undoc-members:
   :show-inheritance:
