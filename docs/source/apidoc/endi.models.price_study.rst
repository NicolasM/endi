endi.models.price\_study package
================================

Submodules
----------

endi.models.price\_study.base module
------------------------------------

.. automodule:: endi.models.price_study.base
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.chapter module
---------------------------------------

.. automodule:: endi.models.price_study.chapter
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.discount module
----------------------------------------

.. automodule:: endi.models.price_study.discount
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.price\_study module
--------------------------------------------

.. automodule:: endi.models.price_study.price_study
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.product module
---------------------------------------

.. automodule:: endi.models.price_study.product
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.services module
----------------------------------------

.. automodule:: endi.models.price_study.services
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.work module
------------------------------------

.. automodule:: endi.models.price_study.work
   :members:
   :undoc-members:
   :show-inheritance:

endi.models.price\_study.work\_item module
------------------------------------------

.. automodule:: endi.models.price_study.work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.models.price_study
   :members:
   :undoc-members:
   :show-inheritance:
