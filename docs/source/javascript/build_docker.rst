Compilation javascript avec docker-compose
============================================

Des fichiers de configuration sont fournis pour permettre la compilation
des ressources Javascript à l'aide d'image docker.


Pré-requis
-----------

- Disposer de docker et docker-compose


Compiler les fichiers une seule fois (dev et prod)
--------------------------------------------------

.. code-block::

     docker-compose -f js-docker-compose.yaml run --rm webpack-vue npm run dev
     docker-compose -f js-docker-compose.yaml run --rm webpack-marionette npm run dev
     docker-compose -f js-docker-compose.yaml run --rm webpack-vue npm run prod
     docker-compose -f js-docker-compose.yaml run --rm webpack-marionette npm run prod


Compilation avec rechargement dynamique (pour le développement Js)
-------------------------------------------------------------------

.. code-block::

    docker-compose -f js-docker-compose.yaml up
