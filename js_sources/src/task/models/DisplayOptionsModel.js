import BaseModel from '../../base/models/BaseModel'

const DisplayOptionsModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: [
        'id',
        'display_units',
        'display_ttc',
        'input_mode', // Mode de saisie du formulaire (étude de prix ou libre)
    ],
})
export default DisplayOptionsModel;